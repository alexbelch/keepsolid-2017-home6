-- MySQL Script generated by MySQL Workbench
-- 07/24/17 12:09:00
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema keepsolid2017
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `keepsolid2017` ;

-- -----------------------------------------------------
-- Schema keepsolid2017
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `keepsolid2017` DEFAULT CHARACTER SET utf8 ;
USE `keepsolid2017` ;

-- -----------------------------------------------------
-- Table `keepsolid2017`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `keepsolid2017`.`users` ;

CREATE TABLE IF NOT EXISTS `keepsolid2017`.`users` (
  `id` INT NOT NULL,
  `fname` VARCHAR(45) NULL,
  `lname` VARCHAR(45) NULL,
  `admin` TINYINT(1) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `keepsolid2017`.`emails`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `keepsolid2017`.`emails` ;

CREATE TABLE IF NOT EXISTS `keepsolid2017`.`emails` (
  `id` INT NOT NULL,
  `email` VARCHAR(45) NULL,
  `main` VARCHAR(45) NULL,
  `user_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_emails_users_idx` (`user_id` ASC),
  CONSTRAINT `fk_emails_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `keepsolid2017`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `keepsolid2017`.`passwords`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `keepsolid2017`.`passwords` ;

CREATE TABLE IF NOT EXISTS `keepsolid2017`.`passwords` (
  `id` INT NOT NULL,
  `password` VARCHAR(15) NULL,
  `email_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_passwords_emails_idx` (`email_id` ASC),
  CONSTRAINT `fk_passwords_emails`
    FOREIGN KEY (`email_id`)
    REFERENCES `keepsolid2017`.`emails` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `keepsolid2017`.`books`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `keepsolid2017`.`books` ;

CREATE TABLE IF NOT EXISTS `keepsolid2017`.`books` (
  `id` INT NOT NULL,
  `name` VARCHAR(250) NULL,
  `isbn` CHAR(13) NULL,
  `description` VARCHAR(250) NULL,
  `year_of_publication` DATE NULL,
  `pages_count` INT NULL,
  `format` ENUM('pdf', 'html') NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `keepsolid2017`.`users_books`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `keepsolid2017`.`users_books` ;

CREATE TABLE IF NOT EXISTS `keepsolid2017`.`users_books` (
  `id` INT NOT NULL,
  `user_id` INT NULL,
  `book_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_autorsbooks_users_idx` (`user_id` ASC),
  INDEX `fk_authorsbooks_books_idx` (`book_id` ASC),
  CONSTRAINT `fk_authorsbooks_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `keepsolid2017`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_authorsbooks_books`
    FOREIGN KEY (`book_id`)
    REFERENCES `keepsolid2017`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `keepsolid2017`.`authors_books`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `keepsolid2017`.`authors_books` ;

CREATE TABLE IF NOT EXISTS `keepsolid2017`.`authors_books` (
  `id` INT NOT NULL,
  `user_id` INT NULL,
  `books_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk__usersbooks_users_idx` (`user_id` ASC),
  INDEX `fk_usersbooks_books_idx` (`books_id` ASC),
  CONSTRAINT `fk__usersbooks_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `keepsolid2017`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usersbooks_books`
    FOREIGN KEY (`books_id`)
    REFERENCES `keepsolid2017`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
